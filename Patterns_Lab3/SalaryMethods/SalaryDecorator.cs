﻿using Patterns_Lab3.Models;

namespace Patterns_Lab3.SalaryMethods
{
    public abstract class SalaryDecorator : BaseSalary
    {
        protected BaseSalary _baseSalary { get; set; }

        public SalaryDecorator(BaseSalary baseSalary)
        {
            _baseSalary = baseSalary;
        }

        public override int GetSalary(Post post)
        {
            if (_baseSalary != null)
            {
                return _baseSalary.GetSalary(post);
            }

            return 0;
        }
    }
}
﻿using Patterns_Lab3.Models;

namespace Patterns_Lab3.SalaryMethods
{
    public class SalaryWithFixedPremium : SalaryDecorator
    {
        public SalaryWithFixedPremium(BaseSalary baseSalary) : base(baseSalary)
        {
        }

        public override int GetSalary(Post post)
        {
            return post.IsFixedPremium
                ? base.GetSalary(post) + post.FixedPremiumSize
                : base.GetSalary(post);
        }
    }
}
﻿using Patterns_Lab3.Models;

namespace Patterns_Lab3.SalaryMethods
{
    public abstract class BaseSalary
    {
        public abstract int GetSalary(Post post);
    }
}
﻿using Patterns_Lab3.Models;

namespace Patterns_Lab3.SalaryMethods
{
    public class SalaryWithPossibleAward : SalaryDecorator
    {
        public SalaryWithPossibleAward(BaseSalary baseSalary) : base(baseSalary)
        {
        }

        public override int GetSalary(Post post)
        {
            return (post.IsPossibleBonus)
                ? base.GetSalary(post) + post.PossibleBonusPercent
                : base.GetSalary(post);
        }
    }
}
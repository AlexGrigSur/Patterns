﻿using Patterns_Lab3.Models;

namespace Patterns_Lab3.SalaryMethods
{
    public class Salary : BaseSalary
    {
        public override int GetSalary(Post post)
        {
            return post.FixedSalary;
        }
    }
}
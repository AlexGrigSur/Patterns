﻿using System;
using Patterns_Lab3.Models;

namespace Patterns_Lab3.SalaryMethods
{
    public class SalaryWithQuarterAward : SalaryDecorator
    {
        public SalaryWithQuarterAward(BaseSalary baseSalary) : base(baseSalary)
        {
        }

        public override int GetSalary(Post post)
        {
            return (post.IsQuarterlyAward && DateTime.UtcNow.Month % 3 == 0)
                ? base.GetSalary(post) * post.QuarterlyAwardSize / 100
                : base.GetSalary(post);
        }
    }
}
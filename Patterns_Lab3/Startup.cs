﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Patterns_Lab3.Controllers;
using Patterns_Lab3.Models;
using Patterns_Lab3.Settings;

namespace Patterns_Lab3
{
    public class Startup
    {
        private IConfiguration _configuration { get; set; }
        public async Task Start()
        {
            _configuration = ReadConfiguration();
            var dbConfig = _configuration.GetSection("DBConfiguration").Get<DBConfiguration>();

            new Controller().ChooseTerminal();
            //await new TerminalViewEmployeeList().ChooseFunction();
        }

        //private async Task<bool> IsDataBaseAvailable(DBConfiguration dbConfig)
        //{
        //    try
        //    {
        //        await DBInit.CreateDataBase(dbConfig);
        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}

        //private async Task<(DateTime lastUpdateTime, DeserializedData data)> ChooseSource(DBConfiguration dbConfig)
        //{
        //    var isDatabaseAvailable = await IsDataBaseAvailable(dbConfig);
        //    var localData = await GetSerializedData();

        //    if (isDatabaseAvailable)
        //    {
        //        var remoteData = new DeserializedData
        //        {
        //            Departments = 
        //        }
        //    }
        //    if (isDatabaseAvailable)
        //    {
        //        Console.WriteLine("Database is available. Beginning to compare local source and remote repository");
                
        //        //return await EmployeeRepository.SelectEmployeesAsync();
        //    }
        //}

        //private async Task<(DateTime lastUpdateTime, DeserializedData data)> GetSerializedData()
        //{
        //    try
        //    {
        //        var employees = await new SaveJson<Employee>().GetList();
        //        var posts = await new SaveJson<Post>().GetList();
        //        var departments = await new SaveJson<Department>().GetList();

        //        return (employees.CreationDate, new DeserializedData()
        //        {
        //            Departments = departments.List,
        //            Posts = posts.List,
        //            Employees = employees.List
        //        });
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        throw;
        //    }
        //}

        private IConfiguration ReadConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json", false)
                .Build();
        }

        //private struct DeserializedData
        //{
        //    public List<Employee> Employees { get; set; }
        //    public List<Post> Posts { get; set; }
        //    public List<Department> Departments { get; set; }
        //}
    }
}
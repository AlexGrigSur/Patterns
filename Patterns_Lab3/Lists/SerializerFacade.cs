﻿using System.Threading.Tasks;
using Patterns_Lab3.Models;
using Patterns_Lab3.SaveEmployees;

namespace Patterns_Lab3.Lists
{
    public static class SerializerFacade
    {
        private const string Departments = "Departments.json";
        private const string Posts = "Post.json";
        private const string Employee = "Employee.json";
        public static async Task Serialize(DepartmentList departmentList, PostList postList, EmployeeList employeeList)
        {
            await employeeList.SaveLocal(new LocalSaveJson<Employee>());
            await departmentList.Save(new LocalSaveJson<Department>());
            await postList.Save(new LocalSaveJson<Post>());
        }
    }
}
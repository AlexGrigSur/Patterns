﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Patterns_Lab3.Lists;
using Patterns_Lab3.Repositories;
using Patterns_Lab3.SaveEmployees;
using Patterns_Lab3.SearchEmployee;

namespace Patterns_Lab3
{
    public class EmployeeList : BaseList<Employee>
    {
        private IRepository<Employee> _repository = new EmployeeRepository();

        public override void Change(int id)
        {
            if (id < 0 || id >= _elementsList.Count)
            {
                Console.WriteLine("Wrong employee index to edit");
                return;
            }

            var employee = _elementsList[id];

            Console.WriteLine("Choose field to edit");
            Console.WriteLine("[0] FirstName");
            Console.WriteLine("[1] LastName");
            Console.WriteLine("[2] FatherName");
            Console.WriteLine("[3] birthDate");
            Console.WriteLine("[4] passport");
            Console.WriteLine("[5] email");
            Console.WriteLine("[6] phones");
            Console.WriteLine("[7] experience");
            Console.WriteLine("[8] last work place");
            Console.WriteLine("[9] last work position");
            Console.WriteLine("[10] last work salary");
            var editField = Convert.ToInt32(Console.ReadLine());
            switch (editField)
            {
                case 0:
                    {
                        Console.WriteLine("Input new first name");
                        var data = Console.ReadLine();
                        employee.FirstName = data;
                        break;
                    }
                case 1:
                    {
                        Console.WriteLine("Input new last name");
                        var data = Console.ReadLine();
                        employee.LastName = data;
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Input new father name");
                        var data = Console.ReadLine();
                        employee.FatherName = data;
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Input new birthDate");
                        var data = Console.ReadLine();
                        employee.BirthDate = data;
                        break;
                    }
                case 4:
                    {
                        Console.WriteLine("Input new passport");
                        var data = Console.ReadLine();
                        employee.Passport = data;
                        break;
                    }
                case 5:
                    {
                        Console.WriteLine("Input new email");
                        var data = Console.ReadLine();
                        employee.Email = data;
                        break;
                    }
                case 6:
                    {
                        Console.WriteLine("input phones count");
                        var count = Convert.ToInt32(Console.ReadLine());
                        var phones = new List<string>(count);
                        for (int i = 0; i < count; ++i)
                        {
                            Console.WriteLine($"Input phone #{i + 1}/{count}");
                            phones[i] = Console.ReadLine();
                        }
                        break;
                    }
                case 7:
                    {
                        Console.WriteLine("Input new experience");
                        var data = Convert.ToInt32(Console.ReadLine());
                        if (data == 0)
                        {
                            employee.Experience = data;
                            employee.PreviousWorkPlace = string.Empty;
                            employee.PreviousWorkPlacePosition = string.Empty;
                            employee.PreviousWorkPlaceSalary = 0;
                            return;
                        }

                        if (employee.Experience != 0)
                        {
                            Console.WriteLine("Input PreviousWorkPlace");
                            var previousPlace = Console.ReadLine();
                            employee.PreviousWorkPlace = previousPlace;

                            Console.WriteLine("Input PreviousWorkPlace");
                            var previousPosition = Console.ReadLine();
                            employee.PreviousWorkPlacePosition = previousPosition;

                            Console.WriteLine("Input PreviousWorkPlace");
                            var previousSalary = Convert.ToInt32(Console.ReadLine());
                            employee.PreviousWorkPlaceSalary = previousSalary;
                        }
                        break;
                    }
                case 8:
                    {
                        if (employee.Experience == 0)
                        {
                            Console.WriteLine("Employee has no experience to edit this field");
                            return;
                        }

                        Console.WriteLine("Input Previous Work Place");
                        var previousPlace = Console.ReadLine();
                        employee.PreviousWorkPlace = previousPlace;

                        break;
                    }
                case 9:
                    {
                        if (employee.Experience == 0)
                        {
                            Console.WriteLine("Employee has no experience to edit this field");
                            return;
                        }

                        Console.WriteLine("Input Previous Work Place Position");
                        var previousPosition = Console.ReadLine();
                        employee.PreviousWorkPlacePosition = previousPosition;

                        break;
                    }
                case 10:
                    {
                        if (employee.Experience == 0)
                        {
                            Console.WriteLine("Employee has no experience to edit this field");
                            return;
                        }

                        Console.WriteLine("Input Previous salary");
                        var previousSalary = Convert.ToInt32(Console.ReadLine());
                        employee.PreviousWorkPlaceSalary = previousSalary;

                        break;
                    }
                default:
                    {
                        Console.WriteLine("Wrong field index");
                        break;
                    }
            }
        }

        public override async Task SaveToDB()
        {
            await _repository.InsertAsync(_elementsList);
        }

        public void Find(ISearchMethod<Employee> searchMethod)
        {
            var employee = searchMethod.SearchEmployees(_elementsList);
            if (employee == null)
            {
                Console.WriteLine("No record found");
            }
            else
            {
                employee.Print();
            }
        }
        /*Also can be made as strategy pattern, but i'm lazy as fuck to do it*/
        public void SortByExperience()
        {
            _elementsList = _elementsList.OrderBy(x => x.Experience).ToList();
        }
    }
}
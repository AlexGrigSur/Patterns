﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Patterns_Lab3.Models;
using Patterns_Lab3.Repositories;
using Patterns_Lab3.SaveEmployees;

namespace Patterns_Lab3.Lists
{
    public class DepartmentList : IModelList
    {
        private List<Department> _departmentLists { get; set; } = new();
        private IRepository<Department> _departmentsRepository = new DepartmentRepository();

        public void Add(Department department)
        {
            _departmentLists.Add(department);
        }
        public void Delete(int id)
        {
            if (id < 0 || id >= _departmentLists.Count)
            {
                return;
            }
            _departmentLists.RemoveAt(id);
        }
        public async Task Save(ILocalSaveMethods<Department> localSaveMethod)
        {
            await localSaveMethod.Save(_departmentLists);
        }

        public void Change(int id)
        {
            throw new System.NotImplementedException();
        }

        public async Task SaveToDB()
        {
            await _departmentsRepository.InsertAsync(_departmentLists);
        }

        public void ShowAll()
        {
            throw new System.NotImplementedException();
        }
    }
}
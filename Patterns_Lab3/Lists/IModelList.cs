﻿using System.Threading.Tasks;

namespace Patterns_Lab3.Lists
{
    public interface IModelList
    {
        void ShowAll();
        void Change(int id);
        Task SaveToDB();
    }
}
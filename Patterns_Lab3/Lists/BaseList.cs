﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Patterns_Lab3.Models;
using Patterns_Lab3.SaveEmployees;

namespace Patterns_Lab3.Lists
{
    public abstract class BaseList<T> : IModelList
    where T : class, IModel<T>
    {
        protected List<T> _elementsList { get; set; } = new();

        public void Add(T record)
        {
            _elementsList.Add(record);
        }

        public abstract void Change(int employeeId);
        public abstract Task SaveToDB();

        public void Delete(int id)
        {
            if (id >= 0 && id < _elementsList.Count)
            {
                _elementsList.RemoveAt(id);
            }
        }
        public T Show(int id)
        {
            if (id >= 0 && id < _elementsList.Count)
            {
                return _elementsList[id];
            }
            return null;
        }
        public void ShowAll()
        {
            for (int i = 0; i < _elementsList.Count; ++i)
            {
                Console.WriteLine("-------------------");
                Console.WriteLine($"id: {i}");
                _elementsList[i].Print();
            }
        }
        public async Task SaveLocal(ILocalSaveMethods<T> localSaveMethod)
        {
            await localSaveMethod.Save(_elementsList);
        }
        public async Task ReadLocalList(ILocalSaveMethods<T> localSaveMethods)
        {
            _elementsList = (await localSaveMethods.GetList()).List;
        }
    }
}
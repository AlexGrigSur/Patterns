﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Patterns_Lab3.Models;
using Patterns_Lab3.Repositories;
using Patterns_Lab3.SaveEmployees;

namespace Patterns_Lab3.Lists
{
    public class PostList : BaseList<Post>
    {
        private Department _department { get; set; }
        private IRepository<Post> _repository = new PostRepository();

        public void Add(Post post)
        {
            _elementsList.Add(post);
        }

        public override void Change(int employeeId)
        {
            throw new NotImplementedException();
        }

        public override async Task SaveToDB()
        {
            await _repository.InsertAsync(_elementsList);
        }

        public void AddRange(IEnumerable<Post> posts)
        {
            _elementsList.AddRange(posts);
        }

        public void Print()
        {
            for(var i=0;i< _elementsList.Count;i++)
            {
                var record = _elementsList[i];
                record.Print();
                Console.WriteLine();
            }
        }
        public Post Choose(int postId)
        {
            if (postId >= 0 && postId < _elementsList.Count)
            {
                return _elementsList[postId];
            }
            return null;
            
        }

        public void Change(Department department)
        {
            _department = department;
        }
        public void Delete(Post post)
        {
            if (_elementsList.Contains(post))
            {
                _elementsList.Remove(post);
            }
        }

        public async Task Save(ILocalSaveMethods<Post> localSaveMethod)
        {
            await localSaveMethod.Save(_elementsList);
        }
    }
}
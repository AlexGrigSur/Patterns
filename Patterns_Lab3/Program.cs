﻿using System.Threading.Tasks;

namespace Patterns_Lab3
{
    class Program
    {
        static async Task Main(string[] args) => await new Startup().Start();
    }
}
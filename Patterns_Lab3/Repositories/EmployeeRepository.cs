﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Npgsql;
using Patterns_Lab3.DataBase;

namespace Patterns_Lab3.Repositories
{
    public class EmployeeRepository : IRepository<Employee>
    {
        private const string Columns = @"
first_name,
last_name,
father_name,
birth_date,
phone,
email,
passport,
experience,
previous_work_place,
previous_work_position,
previous_work_salary";

        private const string SelectSql = @"
SELECT 
{0}
FROM employee;";

        private const string InsertSql = @"
INSERT INTO employee ( {0} )
VALUES (
@first_name,
@last_name,
@father_name,
@birth_date,
@phone,
@email,
@passport,
@experience,
@previous_work_place,
@previous_work_position,
@previous_work_salary
);";

        private const string DeleteSql = @"
DELETE FROM employee
{0};";

        private const string IdParameter = "WHERE id = @id";

        public async Task<List<Employee>> Select()
        {
            var dbInstance = DBConnect.GetInstance();
            var sql = string.Format(SelectSql, Columns);
            return await dbInstance.ExecuteReaderAsync<Employee>(sql);
        }

        public async Task InsertAsync(Employee record)
        {
            var dbInstance = DBConnect.GetInstance();

            var sql = string.Format(InsertSql, Columns);
            await dbInstance.ExecuteNonQueryAsync(sql,
                new NpgsqlParameter("@first_name", record.FirstName),
                new NpgsqlParameter("@last_name", record.LastName),
                new NpgsqlParameter("@father_name", record.FatherName),
                new NpgsqlParameter("@birth_date", record.BirthDateToDateTime()),
                new NpgsqlParameter("@phone", record.Phone),
                new NpgsqlParameter("@email", record.Email),
                new NpgsqlParameter("@passport", record.Passport),
                new NpgsqlParameter("@experience", record.Experience),
                new NpgsqlParameter("@previous_work_place", record.PreviousWorkPlace),
                new NpgsqlParameter("@previous_work_position", record.PreviousWorkPlacePosition),
                new NpgsqlParameter("@previous_work_salary", record.PreviousWorkPlaceSalary));
        }

        public async Task InsertAsync(IEnumerable<Employee> records)
        {
            var dbInstance = DBConnect.GetInstance();

            var sql = string.Format(InsertSql, Columns);

            foreach (var employee in records)
            {
                await dbInstance.ExecuteNonQueryAsync(sql,
                    new NpgsqlParameter("@first_name", employee.FirstName),
                    new NpgsqlParameter("@last_name", employee.LastName),
                    new NpgsqlParameter("@father_name", employee.FatherName),
                    new NpgsqlParameter("@birth_date", employee.BirthDateToDateTime()),
                    new NpgsqlParameter("@phone", employee.Phone),
                    new NpgsqlParameter("@email", employee.Email),
                    new NpgsqlParameter("@passport", employee.Passport),
                    new NpgsqlParameter("@experience", employee.Experience),
                    new NpgsqlParameter("@previous_work_place", employee.PreviousWorkPlace),
                    new NpgsqlParameter("@previous_work_position", employee.PreviousWorkPlacePosition),
                    new NpgsqlParameter("@previous_work_salary", employee.PreviousWorkPlaceSalary));
            }
        }
        public async Task DeleteAsync(int id)
        {
            var dbInstance = DBConnect.GetInstance();
            var sql = string.Format(DeleteSql, IdParameter);
            await dbInstance.ExecuteNonQueryAsync(sql, new NpgsqlParameter("@id", id));
        }

        public async Task DeleteAllAsync()
        {
            var dbInstance = DBConnect.GetInstance();
            var sql = string.Format(DeleteSql, string.Empty);
            await dbInstance.ExecuteNonQueryAsync(sql);
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Npgsql;
using Patterns_Lab3.DataBase;
using Patterns_Lab3.Models;

namespace Patterns_Lab3.Repositories
{
    public class DepartmentRepository: IRepository<Department>
    {
        private PostRepository postRepository = new ();

        private const string Parameters = @"
department_id,
department_name";

        private const string SelectSql = @"
SELECT 
{0}
FROM departments;";

        private const string InsertSql = @"
INSERT INTO departments(
{0}) 
VALUES (
    @department_id,
    @department_name);";

        private const string DeleteSql = @"
DELETE FROM departments
{0};";

        private const string IdCondition = @"
WHERE department_id = @department_id";

        public async Task<List<Department>> Select()
        {
            var posts = await postRepository.Select();

            var dbInstance = DBConnect.GetInstance();
            var sql = string.Format(SelectSql, Parameters);
            var departments = await dbInstance.ExecuteReaderAsync<Department>(sql);
            foreach (var department in departments)
            {
                department.PostList.AddRange(posts.Where(x=> x.Department.Id == department.Id));
            }

            return departments;
        }

        public async Task InsertAsync(Department record)
        {
            var dbInstance = DBConnect.GetInstance();
            var sql = string.Format(InsertSql, Parameters);
            await dbInstance.ExecuteNonQueryAsync(InsertSql,
                new NpgsqlParameter("@department_id", record.Id),
                new NpgsqlParameter("@department_name", record.DepartmentName));
        }

        public async Task InsertAsync(IEnumerable<Department> records)
        {
            var dbInstance = DBConnect.GetInstance();
            var sql = string.Format(InsertSql, Parameters);
            foreach (var record in records)
            {
                await dbInstance.ExecuteNonQueryAsync(InsertSql,
                    new NpgsqlParameter("@department_id", record.Id),
                    new NpgsqlParameter("@department_name", record.DepartmentName));
            }
        }

        public async Task DeleteAsync(int id)
        {
            var dbInstance = DBConnect.GetInstance();
            var sql = string.Format(DeleteSql, Parameters);
            await dbInstance.ExecuteNonQueryAsync(sql, new NpgsqlParameter("@department_id", id));
        }

        public async Task DeleteAllAsync()
        {
            var dbInstance = DBConnect.GetInstance();
            var sql = string.Format(DeleteSql, string.Empty);
            await dbInstance.ExecuteNonQueryAsync(sql);
        }
    }
}
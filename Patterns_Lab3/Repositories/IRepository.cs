﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Patterns_Lab3.Repositories
{
    public interface IRepository<T> where T : class
    {
        Task<List<T>> Select();
        Task InsertAsync(T record);
        Task InsertAsync(IEnumerable<T> records);
        Task DeleteAsync(int id);
        Task DeleteAllAsync();
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Npgsql;
using Patterns_Lab3.DataBase;
using Patterns_Lab3.Models;

namespace Patterns_Lab3.Repositories
{
    public class PostRepository : IRepository<Post>
    {
        private const string Parameters = @"
pst.post_id,
pst.post_name,
pst.fixed_salary,
pst.is_fixed_premium,
pst.fixed_premium_size,
pst.is_quarterly_award,
pst.quarterly_award_size,
pst.is_possible_bonus,
pst.possible_bonus_percent,
pst.department_id,
pst.employee_id";

        private const string ParametersWithJoin = @"
pst.post_id,
pst.post_name,
pst.fixed_salary,
pst.is_fixed_premium,
pst.fixed_premium_size,
pst.is_quarterly_award,
pst.quarterly_award_size,
pst.is_possible_bonus,
pst.possible_bonus_percent,
dep.department_id,
dep.department_name,
emp.id,
emp.first_name,
emp.last_name,
emp.father_name,
emp.birth_date,
emp.email,
emp.passport,
emp.phone,
emp.experience,
emp.previous_work_place,
emp.previous_work_position,
emp.previous_work_salary";

        private const string SelectSql = @"
SELECT
{0}
FROM post pst
{1};";

        private const string InnerJoin = @"
INNER JOIN departments dep on dep.department_id = pst.department_id
INNER JOIN employee emp on emp.id = pst.employee_id";

        private const string InsertSql = @"
INSERT INTO post 
(
{0}
) VALUES
@post_id,
@post_name,
@fixed_salary,
@is_fixed_premium,
@fixed_premium_size,
@is_quarterly_award,
@quarterly_award_size,
@is_possible_bonus,
@possible_bonus_percent,
@department_id,
@employee_id;";

        private const string DeleteSql = @"
DELETE FROM post
{0};";

        private const string IdCondition = @"WHERE id = @id";

        public async Task<List<Post>> Select()
        {
            var dbInstance = DBConnect.GetInstance();
            var sql = string.Format(SelectSql, ParametersWithJoin, InnerJoin);
            return await dbInstance.ExecuteReaderAsync<Post>(sql);
        }

        public async Task InsertAsync(Post record)
        {
            var dbInstance = DBConnect.GetInstance();

            var sql = string.Format(SelectSql, Parameters, string.Empty);
            await dbInstance.ExecuteNonQueryAsync(sql,
                new NpgsqlParameter("@post_id", record.PostId),
                new NpgsqlParameter("@post_name", record.PostName),
                new NpgsqlParameter("@fixed_salary", record.Salary),
                new NpgsqlParameter("@is_fixed_premium", record.IsFixedPremium),
                new NpgsqlParameter("@fixed_premium_size", record.FixedPremiumSize),
                new NpgsqlParameter("@is_quarterly_award", record.IsQuarterlyAward),
                new NpgsqlParameter("@quarterly_award_size", record.QuarterlyAwardSize),
                new NpgsqlParameter("@is_possible_bonus", record.IsPossibleBonus),
                new NpgsqlParameter("@possible_bonus_percent", record.PossibleBonusPercent),
                new NpgsqlParameter("@department_id", record.Department.Id),
                new NpgsqlParameter("@employee_id", record.Employee.Id));
        }

        public async Task InsertAsync(IEnumerable<Post> records)
        {
            var dbInstance = DBConnect.GetInstance();

            var sql = string.Format(SelectSql, Parameters, string.Empty);
            foreach (var post in records)
            {
                await dbInstance.ExecuteNonQueryAsync(sql,
                    new NpgsqlParameter("@post_id", post.PostId),
                    new NpgsqlParameter("@post_name", post.PostName),
                    new NpgsqlParameter("@fixed_salary", post.Salary),
                    new NpgsqlParameter("@is_fixed_premium", post.IsFixedPremium),
                    new NpgsqlParameter("@fixed_premium_size", post.FixedPremiumSize),
                    new NpgsqlParameter("@is_quarterly_award", post.IsQuarterlyAward),
                    new NpgsqlParameter("@quarterly_award_size", post.QuarterlyAwardSize),
                    new NpgsqlParameter("@is_possible_bonus", post.IsPossibleBonus),
                    new NpgsqlParameter("@possible_bonus_percent", post.PossibleBonusPercent),
                    new NpgsqlParameter("@department_id", post.Department.Id),
                    new NpgsqlParameter("@employee_id", post.Employee.Id));
            }
        }

        public async Task DeleteAsync(int id)
        {
            var dbInstance = DBConnect.GetInstance();
            var sql = string.Format(DeleteSql, IdCondition);
            await dbInstance.ExecuteNonQueryAsync(sql, new NpgsqlParameter("@id", id));
        }

        public async Task DeleteAllAsync()
        {
            var dbInstance = DBConnect.GetInstance();
            var sql = string.Format(DeleteSql, string.Empty);
            await dbInstance.ExecuteNonQueryAsync(sql);
        }
    }
}
﻿using System.Collections.Generic;

namespace Patterns_Lab3.SearchEmployee
{
    public interface ISearchMethod<T>
    {
        T SearchEmployees(List<T> employees);
    }
}
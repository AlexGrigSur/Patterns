﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Patterns_Lab3.SearchEmployee
{
    public class SearchByPassport : ISearchMethod<Employee>
    {
        public Employee SearchEmployees(List<Employee> employees)
        {
            Console.WriteLine("Input passport as search parameter");
            var passport = Console.ReadLine();
            return employees.FirstOrDefault(x => x.Passport == passport);
        }
    }
}
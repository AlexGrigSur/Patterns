﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Patterns_Lab3.SearchEmployee
{
    public class SearchByFIO : ISearchMethod<Employee>
    {
        public Employee SearchEmployees(List<Employee> employees)
        {
            Console.WriteLine("Input firstName as search parameter");
            var firstName = Console.ReadLine();

            Console.WriteLine("Input lastName as search parameter");
            var lastName = Console.ReadLine();

            Console.WriteLine("Input fatherName as search parameter");
            var fatherName = Console.ReadLine();

            return employees.FirstOrDefault(x => x.FirstName == firstName && x.LastName == lastName && x.FatherName == fatherName);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Patterns_Lab3.SearchEmployee
{
    public class SearchByEmail : ISearchMethod<Employee>
    {
        public Employee SearchEmployees(List<Employee> employees)
        {
            Console.WriteLine("Input email as search parameter");
            var email = Console.ReadLine();
            return employees.FirstOrDefault(x=> x.Email == email);
        }
    }
}
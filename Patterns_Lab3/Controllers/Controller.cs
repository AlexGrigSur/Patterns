﻿using System;
using System.Collections.Generic;
using System.Linq;
using Patterns_Lab3.Repositories;
using Patterns_Lab3.TerminalViewList;

namespace Patterns_Lab3.Controllers
{
    public class Controller
    {
        private Dictionary<string, ITerminalStart> _terminals = ReflectionHelper.InitSearchForStrategyMethods<ITerminalStart>();

        public void ChooseTerminal()
        {
            int terminalIndex = -1;
            var terminalsCount = _terminals.Count;

            while (terminalIndex != _terminals.Count)
            {
                Console.WriteLine("Please choose terminal to work with:");
                var keys = _terminals.Keys.ToList();
                for (int i = 0; i < keys.Count; ++i)
                {
                    Console.WriteLine($"[{i}] {keys[i]}]");
                }
                Console.WriteLine($"[{keys.Count}] Exit");
                terminalIndex = Convert.ToInt32(Console.ReadLine());
                if (terminalIndex == terminalsCount)
                {
                    return;
                }

                if (terminalIndex < 0 || terminalIndex > terminalsCount)
                {
                    continue;
                }

                _terminals[keys[terminalIndex]].ChooseFunction();
            }
        }
    }
}
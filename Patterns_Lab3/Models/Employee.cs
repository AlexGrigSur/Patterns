﻿using System;
using System.Data;
using System.Linq;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using Patterns_Lab3.Models;

namespace Patterns_Lab3
{
    public class Employee : IModel<Employee>
    {
        private const string PassportRegex = @"[0-9]{4}\s[0-9]{6}";
        private const string FIORegex = @"^(\s?([а-я|А-Я|ё|Ё|a-z|A-Z]+)(\s(-?)\s([а-я|А-Я|ё|Ё|a-z|A-Z]+))?)";
        private const string EmailRegex = @"/\A[^@]+@([^@\.]+\.)+[^@\.]+\z/";
        private const string PhoneRegex = @"^[7]{1}(-?)[0-9]{3}(-?)[0-9]{3}(-?)[0-9]{2}(-?)[0-9]{2}";

        private string _firstName;
        private string _lastName;
        private string _fatherName;
        private string _birthDate;
        private string _passport;
        private string _phone;
        private string _email;

        public int Id { get; set; }
        public string FirstName
        {
            get => _firstName;
            set
            {
                if (!TryParseFIO(value, out var result))
                {
                    throw new ArgumentException("Wrong name format");
                }
                _firstName = result;
            }
        }
        public string LastName
        {
            get => _lastName;
            set
            {
                if (!TryParseFIO(value, out var result))
                {
                    throw new ArgumentException("Wrong name format");
                }
                _lastName = result;
            }
        }
        public string FatherName
        {
            get => _fatherName;
            set
            {
                if (!TryParseFIO(value, out var result))
                {
                    throw new ArgumentException("Wrong name format");
                }
                _fatherName = result;
            }
        }
        public string BirthDate
        {
            get => _birthDate;
            set
            {
                if (TryParseDate(value, out var resultDate))
                {
                    _birthDate = resultDate;
                    return;
                }
                throw new ArgumentException($"BirthDate {value} wrong format");
            }
        }
        public string Phone
        {
            get => _phone;
            set
            {
                if (!TryParsePhone(value, out var formattedNumber))
                {
                    throw new ArgumentException($"Phone number {value} not valid(correct format: D-DDD-DDDDDDD)");
                }
                _phone = value;
            }
        }
        public string Email
        {
            get => _email;
            set
            {
                if (TryParseEmail(value, out var resultEmail))
                {
                    _email = resultEmail;
                    return;
                }
                throw new ArgumentException($"Email format not valid");
            }
        }
        public string Passport
        {
            get => _passport;
            set
            {
                if (!IsPassportValid(value))
                {
                    throw new ArgumentException($"Passport format not valid (correct format: DDDD DDDDDD)");
                }
                _passport = value;
            }
        }
        public int Experience { get; set; }
        public string PreviousWorkPlace { get; set; }
        public string PreviousWorkPlacePosition { get; set; }
        public int PreviousWorkPlaceSalary { get; set; }

        public Employee()
        {
        }

        /// <summary>
        /// Employee constructor in case of first workplace.
        /// Fields experience, previousWorkPlace, previousWorkPlacePosition, previousWorkPlaceSalary will set on 0 or empty string
        /// </summary>
        public Employee(int id, string firstName, string lastName, string fatherName, string birthDate, string phone, string email, string passport) : this(id, firstName, lastName, fatherName, birthDate, phone, email, passport, 0, string.Empty, string.Empty, 0)
        {
        }

        /// <summary>
        /// Employee constructor. Full version
        /// </summary>
        [JsonConstructor]
        public Employee(int id, string firstName, string lastName, string fatherName, string birthDate, string phone, string email, string passport, int experience, string previousWorkPlace, string previousWorkPlacePosition, int previousWorkPlaceSalary)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            FatherName = fatherName;
            BirthDate = birthDate;
            Phone = phone;
            Email = email;
            Passport = passport;
            Experience = experience;
            PreviousWorkPlace = previousWorkPlace;
            PreviousWorkPlacePosition = previousWorkPlacePosition;
            PreviousWorkPlaceSalary = previousWorkPlaceSalary;
        }

        public Employee InputRecord()
        {
            Console.Write("Id: ");
            var id = Convert.ToInt32(Console.ReadLine());

            Console.Write("First name: ");
            var firstName = Console.ReadLine();

            Console.Write("Last name: ");
            var lastName = Console.ReadLine();

            Console.Write("Father name: ");
            var fatherName = Console.ReadLine();

            Console.Write("Birth date: ");
            var birthDate = Console.ReadLine();

            Console.Write("email: ");
            var email = Console.ReadLine();

            Console.Write("Passport: ");
            var passport = Console.ReadLine();

            Console.Write("Input phone number: ");
            var phone = Console.ReadLine();

            Console.Write("Is has experience? ");
            var isExperience = Convert.ToBoolean(Console.ReadLine());

            var experience = 0;
            var previousPlace = string.Empty;
            var previousPosition = string.Empty;
            var previousSalary = 0;

            if (isExperience)
            {
                Console.Write("Input experience: ");
                experience = Convert.ToInt32(Console.ReadLine());

                Console.Write("Input previous work place: ");
                previousPlace = Console.ReadLine();

                Console.Write("Input previous work position: ");
                previousPlace = Console.ReadLine();

                Console.Write("Input experience: ");
                experience = Convert.ToInt32(Console.ReadLine());
            }

            try
            {
                return new Employee(id, firstName, lastName, fatherName, birthDate, phone, email, passport, experience, previousPlace, previousPosition, previousSalary);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        public void Print()
        {
            Console.WriteLine($"{nameof(Id)}: {Id}");
            Console.WriteLine($"{nameof(FirstName)}: {FirstName}");
            Console.WriteLine($"{nameof(LastName)}: {LastName}");
            Console.WriteLine($"{nameof(FatherName)}: {FatherName}");
            Console.WriteLine($"{nameof(BirthDate)}: {BirthDate}");
            Console.WriteLine($"{nameof(Passport)}: {Passport}");
            Console.WriteLine($"{nameof(Email)}: {Email}");
            Console.WriteLine($"{nameof(Phone)}: {Phone}");

            if (this.Experience != 0)
            {
                Console.WriteLine($"{nameof(Experience)}: {Experience}");
                Console.WriteLine($"{nameof(PreviousWorkPlace)}: {PreviousWorkPlace}");
                Console.WriteLine($"{nameof(PreviousWorkPlacePosition)}: {PreviousWorkPlacePosition}");
                Console.WriteLine($"{nameof(PreviousWorkPlaceSalary)}: {PreviousWorkPlaceSalary}");
            }
        }

        public void FillModel(IDataRecord record)
        {
            FirstName = record.GetString(0);
            LastName = record.GetString(1);
            FatherName = record.GetString(2);
            BirthDate = DateTimeToStringConvertor(record.GetDateTime(3));
            Phone = record.GetString(4);
            Email = record.GetString(5);
            Passport = record.GetString(6);
            Experience = record.GetInt32(7); ;
            PreviousWorkPlace = record.GetString(8);
            PreviousWorkPlacePosition = record.GetString(9);
            PreviousWorkPlaceSalary = record.GetInt32(10);
        }

        public DateTime BirthDateToDateTime()
        {
            var dates = BirthDate.Split('.').Select(x => Convert.ToInt32(x)).ToList();
            return new DateTime(dates[2], dates[1], dates[0]);
        }

        private static bool TryParseDate(string inputDate, out string resultDate)
        {
            resultDate = null;

            string[] dateString = inputDate.Split('.');
            if (dateString.Length != 3)
            {
                return false;
            }

            try
            {
                var dates = dateString.Select(x => Int32.Parse(x)).ToArray();

                if (dates[0] > 31 || dates[0] < 1)
                {
                    return false;
                }

                if (dates[1] > 12 || dates[1] < 1)
                {
                    return false;
                }

                int currentYear = DateTime.Now.Year;
                if (dates[2] > currentYear || dates[2] < currentYear - 200)
                {
                    return false;
                }

                resultDate = $"{dateString[0].PadLeft(2, '0')}.{dateString[1].PadLeft(2, '0')}.{dateString[2]}";
                return DateTime.TryParse($"{dates[2]}/{dates[1]}/{dates[0]}", out _);
            }
            catch (Exception)
            {
                return false;
            }
        }
        private static string DateTimeToStringConvertor(DateTime dateTime)
        {
            var day = dateTime.Day;
            var month = dateTime.Month;
            var year = dateTime.Year;
            return $"{day.ToString().PadLeft(2, '0')}.{month.ToString().PadLeft(2, '0')}.{year}";
        }
        private static bool IsPassportValid(string inputPassport)
        {
            return Regex.IsMatch(inputPassport, PassportRegex);
        }
        private static bool TryParseEmail(string inputEmail, out string resultEmail)
        {
            resultEmail = null;
            if (!Regex.IsMatch(inputEmail, EmailRegex))
            {
                resultEmail = inputEmail.ToLower();
                return true;
            }
            return false;
        }
        private static bool TryParsePhone(string inputPhone, out string resultPhone)
        {
            resultPhone = null;
            if (Regex.IsMatch(inputPhone, PhoneRegex))
            {
                resultPhone = inputPhone;
                return true;
            }
            return false;
        }
        private static bool TryParseFIO(string input, out string resultFIO)
        {
            resultFIO = null;
            if (Regex.IsMatch(input, FIORegex))
            {
                resultFIO = input;
                return true;
            }
            return false;
        }
    }
}
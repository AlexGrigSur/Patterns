﻿using System;
using System.Data;
using System.Runtime.InteropServices;
using Patterns_Lab3.SalaryMethods;

namespace Patterns_Lab3.Models
{
    public class Post : IModel<Post>
    {
        public string PostId { get; set; }
        public string PostName { get; set; }
        public int FixedSalary { get; set; }
        public bool IsFixedPremium { get; set; }
        public int FixedPremiumSize { get; set; }
        public bool IsQuarterlyAward { get; set; }
        public int QuarterlyAwardSize { get; set; }
        public bool IsPossibleBonus { get; set; }
        public int PossibleBonusPercent { get; set; }
        public Department Department { get; set; }
        public Employee Employee { get; set; }
        public Salary Salary { get; } = new();

        public Post InputRecord()
        {
            Console.WriteLine("Input PostId");
            var postId = Console.ReadLine();
            
            Console.WriteLine("Input PostName");
            var postName = Console.ReadLine();

            Console.WriteLine("Input FixedSalary");
            var fixedSalary = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Input IsFixedPremium");
            var isFixedPremium = Convert.ToBoolean(Console.ReadLine());

            Console.WriteLine("Input FixedPremiumSize");
            var fixedPremiumSize = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Input IsQuarterlyAward");
            var isQuarterlyAward = Convert.ToBoolean(Console.ReadLine());

            Console.WriteLine("Input QuarterlyAwardSize");
            var quarterlyAwardSize = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Input IsPossibleBonus");
            var isPossibleBonus = Convert.ToBoolean(Console.ReadLine());

            Console.WriteLine("Input PossibleBonusPercent");
            var possibleBonusPercent = Convert.ToInt32(Console.ReadLine());

            return new Post()
            {
                PostId = postId,
                PostName = postName,
                FixedSalary = fixedSalary,
                IsFixedPremium = isFixedPremium,
                FixedPremiumSize = fixedPremiumSize,
                IsQuarterlyAward = isQuarterlyAward,
                QuarterlyAwardSize = quarterlyAwardSize,
                IsPossibleBonus = isPossibleBonus,
                PossibleBonusPercent = possibleBonusPercent
            };
        }

        public void Print()
        {
            Console.WriteLine($"{nameof(PostId)} : {PostId}");
            Console.WriteLine($"{nameof(PostName)} : {PostName}");
            Console.WriteLine($"{nameof(FixedSalary)} : {FixedSalary}");
            Console.WriteLine($"{nameof(IsFixedPremium)} : {IsFixedPremium}");
            Console.WriteLine($"{nameof(FixedPremiumSize)} : {FixedPremiumSize}");
            Console.WriteLine($"{nameof(IsQuarterlyAward)} : {IsQuarterlyAward}");
            Console.WriteLine($"{nameof(QuarterlyAwardSize)} : {QuarterlyAwardSize}");
            Console.WriteLine($"{nameof(IsPossibleBonus)} : {IsPossibleBonus}");
            Console.WriteLine($"{nameof(PossibleBonusPercent)} : {PossibleBonusPercent}");
            Console.WriteLine($"{nameof(Department.Id)} : {Department.Id}");
        }

        public void FillModel(IDataRecord record)
        {
            PostId = record.GetString(0);
            PostName = record.GetString(1);
            FixedSalary = record.GetInt32(2);
            FixedPremiumSize = record.GetInt32(3);
            IsQuarterlyAward = record.GetBoolean(4);
            QuarterlyAwardSize = record.GetInt32(5);
            IsPossibleBonus = record.GetBoolean(6);
            PossibleBonusPercent = record.GetInt32(7);
            Department = new Department
            {
                Id = record.GetInt32(8),
                DepartmentName = record.GetString(9)
            };
            Employee = new Employee
            {
                FirstName = record.GetString(10),
                LastName = record.GetString(11),
                FatherName = record.GetString(12),
                BirthDate = record.GetDateTime(13).ToString("dd.MM.yyyy"),
                Email = record.GetString(14),
                Passport = record.GetString(15),
                Phone = record.GetString(16),
                Experience = record.GetInt32(17),
                PreviousWorkPlace = record.GetString(18),
                PreviousWorkPlacePosition = record.GetString(19),
                PreviousWorkPlaceSalary = record.GetInt32(20)
            };
        }
    }
}
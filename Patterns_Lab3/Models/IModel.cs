﻿using System.Data;

namespace Patterns_Lab3.Models
{
    public interface IModel<T> where T: class
    {
        T InputRecord();
        void Print();
        void FillModel(IDataRecord record);
    }
}
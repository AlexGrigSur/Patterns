﻿using System;
using System.Data;
using Patterns_Lab3.Lists;

namespace Patterns_Lab3.Models
{
    public class Department : IModel<Department>
    {
        public int Id { get; set; }
        public string DepartmentName { get; set; }
        public PostList PostList { get; set; }

        public Department InputRecord()
        {
            Console.WriteLine("Input Id");
            var id = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Input DepartmentName");
            var departmentName = Console.ReadLine();

            return new Department()
            {
                Id = id,
                DepartmentName = departmentName
            };
        }

        public void Print()
        {
            Console.WriteLine($"{nameof(Id)} : {Id}");
            Console.WriteLine($"{DepartmentName} : {DepartmentName}");
        }

        public void FillModel(IDataRecord record)
        {
            Id = record.GetInt32(0);
            DepartmentName = record.GetString(1);
        }
    }
}
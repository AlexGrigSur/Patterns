﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Patterns_Lab3
{
    public static class ReflectionHelper
    {
        public static Dictionary<string, T> InitSearchForStrategyMethods<T>() where T : class
        {
            var methods = new Dictionary<string, T>();
            var targetType = typeof(T);
            var isTargetTypeGeneric = targetType.IsGenericType;
            var types = Assembly.GetExecutingAssembly().GetTypes();

            foreach (var type in types)
            {
                if (isTargetTypeGeneric && type.IsGenericType)
                {
                    if (type.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == targetType.GetGenericTypeDefinition()))
                    {
                        var method = Activator.CreateInstance(type.MakeGenericType(targetType.GetGenericArguments().First()));
                        methods.Add(method.GetType().Name, (T)method);
                        continue;
                    }
                }
                else
                {
                    if (targetType.IsAssignableFrom(type) && type.IsClass)
                    {
                        var method = Activator.CreateInstance(type);
                        methods.Add(method.GetType().Name, (T)method);
                        continue;
                    }
                }
            }

            return methods;
        }
    }
}
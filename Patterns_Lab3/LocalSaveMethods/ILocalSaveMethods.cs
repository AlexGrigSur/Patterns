﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Patterns_Lab3.Models;

namespace Patterns_Lab3.SaveEmployees
{
    public interface ILocalSaveMethods<T> where T: class, IModel<T> 
    {
        public Task Save(List<T> records);
        public Task<ListWrapper<T>> GetList();
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using System.Threading.Tasks;
using Patterns_Lab3.Models;

namespace Patterns_Lab3.SaveEmployees
{
    public class LocalSaveXml<T> : ILocalSaveMethods<T>
        where T:class, IModel<T>
    {
        private readonly string _path = $"{typeof(T).Name}.xml";

        public Task Save(List<T> records)
        {
            var listWrapper = new ListWrapper<T>()
            {
                CreationDate = DateTime.UtcNow,
                List = records
            };

            var formatter = new XmlSerializer(typeof(ListWrapper<T>));

            using var fs = new FileStream(_path, FileMode.OpenOrCreate);
            formatter.Serialize(fs, listWrapper);

            return Task.CompletedTask;
        }

        public Task<ListWrapper<T>> GetList()
        {
            var formatter = new XmlSerializer(typeof(List<T>));

            using var fs = new FileStream(_path, FileMode.OpenOrCreate);
            return Task.FromResult((ListWrapper<T>)formatter.Deserialize(fs));
        }
    }
}
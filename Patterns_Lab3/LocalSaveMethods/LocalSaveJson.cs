﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using Patterns_Lab3.Models;

namespace Patterns_Lab3.SaveEmployees
{
    public class LocalSaveJson<T> : ILocalSaveMethods<T> 
        where T:class, IModel<T>
    {
        private readonly string _path = $"{typeof(T).Name}.json"; 
        public async Task Save(List<T> records)
        {
            var listEmployeeWrapper = new ListWrapper<T>()
            {
                CreationDate = DateTime.UtcNow,
                List = records
            };
            var serializedData = JsonSerializer.Serialize(listEmployeeWrapper);
            await File.WriteAllTextAsync(_path, serializedData);
        }

        public async Task<ListWrapper<T>> GetList()
        {
            var data = await File.ReadAllTextAsync(_path);
            return JsonSerializer.Deserialize<ListWrapper<T>>(data);
        }
    }
}
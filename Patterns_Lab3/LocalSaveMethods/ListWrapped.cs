﻿using System;
using System.Collections.Generic;
using Patterns_Lab3.Models;

namespace Patterns_Lab3.SaveEmployees
{
    public class ListWrapper<T> where T: class, IModel<T>
    {
        public DateTime CreationDate { get; set; }
        public List<T> List { get; set; }
    }
}
﻿namespace Patterns_Lab3.Settings
{
    public class DBConfiguration
    {
        public string Server { get; set; }
        public int Port { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public string Database { get; set; }
    }
}
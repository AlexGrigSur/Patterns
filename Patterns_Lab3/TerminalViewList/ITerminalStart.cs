﻿using System.Threading.Tasks;

namespace Patterns_Lab3.TerminalViewList
{
    public interface ITerminalStart
    {
        Task ChooseFunction();
    }
}
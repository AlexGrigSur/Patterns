﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Patterns_Lab3.Lists;
using Patterns_Lab3.Models;
using Patterns_Lab3.SaveEmployees;
using Patterns_Lab3.SearchEmployee;

namespace Patterns_Lab3.TerminalViewList
{
    public class TerminalViewDepartmentList : TerminalViewList<DepartmentList, Department>, ITerminalStart
    {
        private DepartmentList _departmentList { get; } = new();

        public TerminalViewDepartmentList()
        {
            _list = new();
            _searchMethods = ReflectionHelper.InitSearchForStrategyMethods<ISearchMethod<Department>>();
            _saveMethods = ReflectionHelper.InitSearchForStrategyMethods<ILocalSaveMethods<Department>>();
        }

        public TerminalViewDepartmentList(DepartmentList sourceList)
        {
            _list = sourceList;
            _searchMethods = ReflectionHelper.InitSearchForStrategyMethods<ISearchMethod<Department>>();
            _saveMethods = ReflectionHelper.InitSearchForStrategyMethods<ILocalSaveMethods<Department>>();
        }

        public async Task ChooseFunction()
        {
            while (true)
            {
                Console.WriteLine("Choose action");
                Console.WriteLine("[0] Add Department");
                Console.WriteLine("[NSE] Edit Department");
                Console.WriteLine("[2] Delete Department");
                Console.WriteLine("[4] Print Departments");
                Console.WriteLine("[5] Add some test employees");
                Console.WriteLine("[NSE] Save Employees");
                Console.WriteLine("[NSE] Get Employees");
                Console.WriteLine("[8] Sort employees by experience");
                Console.WriteLine("[9] Exit");

                Console.WriteLine();

                switch (Convert.ToInt32(Console.ReadLine()))
                {
                    case 0:
                        {
                            Add();
                            break;
                        }
                    case 1:
                        {
                            Console.WriteLine("NotSupported");//Change();
                            break;
                        }
                    case 2:
                        {
                            Delete();
                            break;
                        }
                    case 3:
                        {
                            Show();
                            break;
                        }
                    case 4:
                        {
                            await LocalSave();
                            break;
                        }
                    case 5:
                        {
                            //await GetEmployees();
                            break;
                        }
                    case 6:
                        {
                            //SortByExperience();
                            break;
                        }
                    case 7:
                        {
                            return;
                        }
                    default:
                        {
                            Console.WriteLine("Wrong action");
                            break;
                        }
                }

                Console.WriteLine();
                Console.WriteLine();
            }
        }

        private async Task LocalSave()
        {
            var saveMethodsKeys = _saveMethods.Keys.ToList();
            for (int i = 0; i < saveMethodsKeys.Count; ++i)
            {
                Console.WriteLine($"[{i}] {saveMethodsKeys[i]}");
            }

            try
            {
                Console.WriteLine("Choose save method");
                var searchMethodIndex = Convert.ToInt32(Console.ReadLine());

                var saveMethod = _saveMethods[saveMethodsKeys[searchMethodIndex]];
                await _list.Save(saveMethod);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void Add()
        {
            try
            {
                var record = new Department().InputRecord();
                _list.Add(record);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        //private void Change()
        //{
        //    Console.WriteLine("Input employee id to edit");

        //    try
        //    {
        //        var removeId = Convert.ToInt32(Console.ReadLine());
        //        _list.(removeId);
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //    }
        //}
        private void Delete()
        {
            Console.WriteLine("Input employee id to delete");

            try
            {
                var removeId = Convert.ToInt32(Console.ReadLine());
                _list.Delete(removeId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

    }
}
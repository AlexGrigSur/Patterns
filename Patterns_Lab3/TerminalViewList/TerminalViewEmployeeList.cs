﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Patterns_Lab3.SaveEmployees;
using Patterns_Lab3.SearchEmployee;
using Patterns_Lab3.TerminalViewList;

namespace Patterns_Lab3
{
    public class TerminalViewEmployeeList : TerminalViewList<EmployeeList, Employee>
    {

        public TerminalViewEmployeeList()
        {
            _list = new();
            _searchMethods = ReflectionHelper.InitSearchForStrategyMethods<ISearchMethod<Employee>>();
            _saveMethods = ReflectionHelper.InitSearchForStrategyMethods<ILocalSaveMethods<Employee>>();
        }

        public TerminalViewEmployeeList(EmployeeList sourceList)
        {
            _list = sourceList;
            _searchMethods = ReflectionHelper.InitSearchForStrategyMethods<ISearchMethod<Employee>>();
            _saveMethods = ReflectionHelper.InitSearchForStrategyMethods<ILocalSaveMethods<Employee>>();
        }
        public async Task ChooseFunction()
        {
            while (true)
            {
                Console.WriteLine("Choose action");
                Console.WriteLine("[0] Add Employee");
                Console.WriteLine("[1] Edit Employee");
                Console.WriteLine("[2] Delete Employee");
                Console.WriteLine("[3] Search Employee");
                Console.WriteLine("[4] Print Employees");
                Console.WriteLine("[5] Add some test employees");
                Console.WriteLine("[6] Save Employees");
                Console.WriteLine("[7] Get Employees");
                Console.WriteLine("[8] Sort employees by experience");
                Console.WriteLine("[9] Exit");

                Console.WriteLine();

                switch (Convert.ToInt32(Console.ReadLine()))
                {
                    case 0:
                        {
                            Add();
                            break;
                        }
                    case 1:
                        {
                            Change();
                            break;
                        }
                    case 2:
                        {
                            Delete();
                            break;
                        }
                    case 3:
                        {
                            Find();
                            break;
                        }
                    case 4:
                        {
                            Show();
                            break;
                        }
                    case 5:
                        {
                            AddSomeTestEmployees();
                            break;
                        }
                    case 6:
                        {
                            await Save();
                            break;
                        }
                    case 7:
                        {
                            await GetEmployees();
                            break;
                        }
                    case 8:
                        {
                            SortByExperience();
                            break;
                        }
                    case 9:
                        {
                            return;
                        }
                    default:
                        {
                            Console.WriteLine("Wrong action");
                            break;
                        }
                }

                Console.WriteLine();
                Console.WriteLine();
            }
        }
        private void Add()
        {
            try
            {
                var employee = new Employee().InputRecord();
                _list.Add(employee);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        private void Change()
        {
            Console.WriteLine("Input employee id to edit");

            try
            {
                var removeId = Convert.ToInt32(Console.ReadLine());
                _list.Change(removeId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        private void Delete()
        {
            Console.WriteLine("Input employee id to delete");

            try
            {
                var removeId = Convert.ToInt32(Console.ReadLine());
                _list.Delete(removeId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        private void Show()
        {
            _list.ShowAll();
        }
        private void AddSomeTestEmployees()
        {
            var random = new Random();
            _list.Add(new Employee(random.Next(1000000, 9999999), "Alex", "Grig", "Sur", "22.08.2000", $"7-900-{random.Next(1000000, 9999999)}", $"alex.grig.as{random.Next(100000, 9999999)}ur@gmail.com", $"1111 {random.Next(100000, 999999)}"));
            _list.Add(new Employee(random.Next(1000000, 9999999), "Alex", "Grig", "Test", "22.08.2000", $"7-900-{random.Next(1000000, 9999999)}", $"alex.grig.re{random.Next(100000, 9999999)}ur@gmail.com", $"1111 {random.Next(100000, 999999)}", 1, "Plarium", "Intern C# Developer", 10000));
        }
        private void SortByExperience()
        {
            _list.SortByExperience();
        }
        private void Find()
        {
            var searchMethodsKeys = _searchMethods.Keys.ToList();
            for (int i = 0; i < searchMethodsKeys.Count; ++i)
            {
                Console.WriteLine($"[{i}] {searchMethodsKeys[i]}");
            }

            try
            {
                Console.WriteLine("Choose search method");
                var searchMethodIndex = Convert.ToInt32(Console.ReadLine());

                var searchMethod = _searchMethods[searchMethodsKeys[searchMethodIndex]];
                _list.Find(searchMethod);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        private async Task Save()
        {
            var saveMethodsKeys = _saveMethods.Keys.ToList();
            for (int i = 0; i < saveMethodsKeys.Count; ++i)
            {
                Console.WriteLine($"[{i}] {saveMethodsKeys[i]}");
            }

            try
            {
                Console.WriteLine("Choose save method");
                var searchMethodIndex = Convert.ToInt32(Console.ReadLine());

                var saveMethod = _saveMethods[saveMethodsKeys[searchMethodIndex]];
                await _list.SaveLocal(saveMethod);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        private async Task GetEmployees()
        {
            var saveMethodsKeys = _saveMethods.Keys.ToList();
            for (int i = 0; i < saveMethodsKeys.Count; ++i)
            {
                Console.WriteLine($"[{i}] {saveMethodsKeys[i]}");
            }

            try
            {
                Console.WriteLine("Choose save method");
                var searchMethodIndex = Convert.ToInt32(Console.ReadLine());

                var saveMethod = _saveMethods[saveMethodsKeys[searchMethodIndex]];
                await _list.ReadLocalList(saveMethod);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Patterns_Lab3.Lists;
using Patterns_Lab3.Models;
using Patterns_Lab3.SaveEmployees;
using Patterns_Lab3.SearchEmployee;

namespace Patterns_Lab3.TerminalViewList
{
    public class TerminalViewList<TList, TModel>
        where TList : class, IModelList, new()
        where TModel : class, IModel<TModel>, new()
    {
        protected TList _list { get; set; } = new TList();
        protected Dictionary<string, ISearchMethod<TModel>> _searchMethods { get; set; }
        protected Dictionary<string, ILocalSaveMethods<TModel>> _saveMethods { get; set; }
        protected void Show()
        {
            _list.ShowAll();
        }
    }
}
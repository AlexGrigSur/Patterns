﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Npgsql;
using Patterns_Lab3.Models;

namespace Patterns_Lab3.DataBase
{
    public class DBConnect
    {
        private readonly NpgsqlConnection _connection;
        private readonly NpgsqlCommand _command;
        protected static DBConnect Instance = null;

        private DBConnect(string connectionString)
        {
            _connection = new NpgsqlConnection(connectionString);
            _command = new NpgsqlCommand();
        }
        private void OpenConnection()
        {
            if (_connection.State == System.Data.ConnectionState.Closed)
                _connection.Open();
        }

        private void CloseConnection()
        {
            if (_connection.State == System.Data.ConnectionState.Open)
                _connection.Close();
        }

        public static DBConnect GetInstance(string connectionString = null)
        {
            if (Instance is null || connectionString is not null)
            {
                Instance = new DBConnect(connectionString);
                return Instance;
            }
            return Instance;
        }
        /// <summary>
        /// Execute SQL command
        /// </summary>
        /// <param name="SQLcommand">sql command</param>
        /// <param name="paramsColl">parameters list</param>
        public async Task ExecuteNonQueryAsync(string SQLcommand, params NpgsqlParameter[] parameters)
        {
            _command.CommandText = SQLcommand;
            _command.Connection = _connection;
            _command.Parameters.Clear();

            if (parameters.Length != 0)
            {
                foreach (var parameter in parameters)
                {
                    _command.Parameters.Add(parameter);
                }
            }

            OpenConnection();
            await _command.ExecuteNonQueryAsync();
            CloseConnection();
        }
        /// <summary>
        /// Execute SQL DataReader
        /// </summary>
        /// <param name="sql">sql command</param>
        /// <param name="paramsColl">parameters list</param>
        /// <returns></returns>
        public async Task<List<T>> ExecuteReaderAsync<T>(string sql, params NpgsqlParameter[] paramsColl)
            where T : class, IModel<T>, new()
        {
            _command.CommandText = sql;
            _command.Connection = _connection;
            _command.Parameters.Clear();

            if (paramsColl != null)
            {
                foreach (var i in paramsColl)
                {
                    _command.Parameters.Add(i);
                }
            }

            var result = new List<T>();

            OpenConnection();
            await using (var reader = await _command.ExecuteReaderAsync())
            {
                while (await reader.ReadAsync())
                {
                    var record = new T();
                    record.FillModel(reader);
                    result.Add(record);
                }
            }
            CloseConnection();

            return result;
        }
    }
}
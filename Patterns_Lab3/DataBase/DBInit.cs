﻿using System.Threading.Tasks;
using Patterns_Lab3.Settings;

namespace Patterns_Lab3.DataBase
{
    public class DBInit
    {
        public static async Task CreateDataBase(DBConfiguration config)
        {
            var dbInstance = DBConnect.GetInstance($"Server={config.Server};Port={config.Port};User ID={config.User};Password={config.Password};Database=postgres;");

            //await dbInstance.ExecuteNonQueryAsync($"create database {config.Database};");

            dbInstance = DBConnect.GetInstance($"Server={config.Server};Port={config.Port};User ID={config.User};Password={config.Password};Database={config.Database};");

            await dbInstance.ExecuteNonQueryAsync("create table if not exists employee(" +
                                                  "id SERIAL not null primary key," +
                                                  "first_name varchar(150) not null," +
                                                  "last_name varchar(150) not null," +
                                                  "father_name varchar(150) not null," +
                                                  "birth_date timestamp not null," +
                                                  "email varchar(150) not null unique," +
                                                  "passport varchar(150) not null unique," +
                                                  "phone varchar(150) not null unique," +
                                                  "experience int not null," +
                                                  "previous_work_place varchar(150)," +
                                                  "previous_work_position varchar(150)," +
                                                  "previous_work_salary int);");

            await dbInstance.ExecuteNonQueryAsync("create table if not exists departments(" +
                                                  "department_id SERIAL not null primary key," +
                                                  "department_name varchar(150) not null);");

            await dbInstance.ExecuteNonQueryAsync("create table if not exists post(" +
                                                  "post_id SERIAL not null primary key," +
                                                  "post_name varchar(150) not null," +
                                                  "fixed_salary int not null" +
                                                  "is_fixed_premium boolean not null," +
                                                  "fixed_premium_size int not null," +
                                                  "is_quarterly_award boolean not null," +
                                                  "quarterly_award_size int not null," +
                                                  "is_possible_bonus boolean not null," +
                                                  "possible_bonus_percent int not null," +
                                                  "department_id serial not null references departments (department_id)," +
                                                  "employee_id serial not null references employee (id));");
        }
    }
}